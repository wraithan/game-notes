1. Power
    - [ ] Basic Solar
    - [ ] 2 axis tracking solar
    - [ ] Station batteries
    - [ ] Wind Turbines (storm power)
    - [ ] Gas Fuel Generator
2. Shelter
    - [ ] Air tight 
    - [ ] Manual airlock
    - [ ] Automatic airlock
    - [ ] Lights
3. Water
    - [ ] Ice crusher / bottle filler
    - [ ] Temperature controlled water storage
4. Air
    - [ ] Atmospherics
        - [ ] Toxic out
        - [ ] N2 in
        - [ ] O2 in
        - [ ] Temperature management
    - [ ] Bulk O2 storage
    - [ ] Gas Capture
5. Food
    - [ ] Grow in portable hydroponics
    - [ ] Large fridge
    - [ ] Piped manual hydroponics
6. Tools
    - [ ] Furnace
        - [ ] Steel
        - [ ] Solder
    - [ ] Welding Fuel

