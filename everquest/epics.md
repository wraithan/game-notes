# Monk

## 1.0

Resources:
* [EQ Progression Guide](https://www.eqprogression.com/monk-1-0-epic-quest/)

<details>
<summary>Why</summary>

* 1.5/2.0
* Nostalgia

</details>

<details>
<summary>Notes</summary>

Most of the camps seem really reasonable, may need 60 for some bits.

</details>

<details>
<summary>Checklist</summary>

- [ ] Robe of the Whistling Fist - Dreadlands give to Brother Balatin (KoS, give all items)
    - [ ] **Robe of the Lost Circle** - Rathe Mountains from Brother Zephyl
    - [ ] **Metal Pipe (Zan)** - Karnor's Castle from drolvarg pawbuster
    - [ ] **Metal Pipe (Fi)** - Chardok from iksar betrayer
- [ ] Demon Fangs - Lake of Ill Omen from Xenevorash (Give Eye of Kaiaren to Astral Projection then fight)
    - [ ] **Charred Scale** - Lavastorm Mountains from Eejag ("I challenge eejag" to a fire sprite then fight)
    - [ ] Breath of Gwan - Plane of Sky from Gwan (Give Charred Scale then fight)
    - [ ] Trunt's Head - Nurga from Trunt (Give Breath of Gwan to a sleeping ogre then fight)
    - [ ] Eye of Kaiaren - Overthere from Astral Projection (Give Trunt's Head)
- [ ] Celestial Fists (Book) - Timorous Deep from Lheao (give all items)
    - [ ] **Immortals** - Skyfire from "named mobs"
        - a lava walker
        - a shadow drake
        - a soul devourer
        - a wandering wurm
        - a wurm spirit
        - Black Scar
        - Faerie of Dismay
        - Guardian of Felia
    - [ ] Danl's Reference - Erudin from Tomekeeper Danl (Give Immortals)
- [ ] Celestials Fists - Trakanon's Teeth from Kaiaren (Sane, Give Book of Celestial Fists and Demon Fangs)
    - [ ] Celestial Fists (Book) - Trakanon's Teeth from Kaiaren (Crazy, give Celestial Fists (Book), in SE near Old Sebilis then fight/run)
    - [ ] Book of Celestial Fists - Trakanon's Teeth from Kaiaren (Sane, give Celestial Fists (Book), in center left of zone in hut)

</details>

# Shaman

## 1.0

Resources:
* [EQ Progression Guide](https://www.eqprogression.com/shaman-1-0-epic-quest/)

<details>
<summary>Why</summary>

* 1.5/2.0

</details>

<details>
<summary>Notes</summary>

</details>

<details>
<summary>Checklist</summary>

- [ ] 1 - Tiny Gem from a lesser spirit (Kill one of the following, then Say "i wish to know more")
    - Ocean of Tears - Captain Surestout
    - Butcherblock Mountains - Peg Leg
    - Field of Bone - Iksar Manslayer
    - Rathe Mountains - Blinde the Cutpurse
- [ ] 2 - Opaque Gem
    1. West Freeport - find Bondl Felligan
    2. Give Tiny Gem
    3. Say "you can buy booze"
    4. Say "Heyokah" to greater spirit (get Opaque Gem)
    5. Say "Walk the path" to greater spirit
- [ ] 3 - Small Gem - Erud's Crossing (Takes 28+ minutes with spawns/pathing/waiting)
    1. Ooglyn give Opaque Gem, follow
    2. Srafen the Soaked say "here", "wait", "us", "else"
    3. Dillon the Drowned say "no", "disasterous"
    4. Froham the Forgotten say "who are you", "arrow"?
    5. Abe the Abandoned say "a broken arrow", "i will give them the arrow"
    6. a greater spirit say "Hail", "next path"
- [ ] 4 - Rathe Mountains
    - [ ] **Marr's Purpose** from Tambien the Goodly
    - [ ] **Envy** from Glaron the Wicked
    - [ ] **Woe** from Glaron the Wicked
- [ ] 5 - Sparkling Gem - West Karana from a wandering spirit
    1. Give Small Gem
    2. Give Marr's Promise, Envy, Woe
- [ ] 6 - Emerald Jungle - Spirit Sentinel give Sparkling Gem
- [ ] 7 - Blake Dire Pelt - Castle Mistmore from Black Dire (say "i will not join you" then fight)
- [ ] 8 - Ancient Journel - Emerald Jungle from Spirit Sentinel
    1. Give Dire Pelt
    2. say "how it all began"
- [ ] 9 - Completed Report combine the following in the Ancient Journal
    - City of Mist
    - [ ] **Crier's Scroll** - Trash
    - [ ] **Merchant's Letter** - Trash
    - [ ] **Personal Diary Page** - Trash
    - [ ] **Priest's Diary Page** - Trash
    - [ ] **Student's Log** - spectral courier
    - [ ] **Written Announcement** - Trash
- [ ] 10 - City of Mist kill Lord Ghiosk
    1. Get Stone Key
    2. Kill Black Rever
    3. Kill Lord Ghiosk
    - [ ] **Crusades of the High Scale**
    - [ ] **Head Housekeeper's Log**
    - [ ] **Historic Article**
- [ ] 11 - **Icon of the High Scale** - City of Mist ground spawn
- [ ] 12 - Emerald Jungle give Spirit Sentinel the step 10 drops
- [ ] 13 - Engraved Ring - The Hole from High Scale Kirn (Give icon, fight)
- [ ] 14 - Neh'Ashilir's Diary - City of Mist
    1. Kill black rever
    2. Give Neh'Ashilir the Engraved Ring
    3. Kill and loot
    - Note, drops a temporary key for a future fight
- [ ] 15 - Emerald Jungle - Spirit Sentinel give Neh'Ashilir's Diary
- [ ] 16 - **Child's Tear** - Plane of Fear from Dread, Fright, or Terror
    - Spawns Iksar Broodling that drops the tear
- [ ] 17 - Iksar Scale - City of Mist
    1. Kill Black Rever
    2. Give Lord Rak'ashirr the Child's Tear
    3. Kill and loot
- [ ] 18 - Spear of Fate - Emerald Jungle from Spirit Sentinel (Give Iksar Scale)

</details>

# Bard

## 1.0

Resources:
* [EQ Progression Guide](https://www.eqprogression.com/bard-1-0-epic-quest/)

<details>
<summary>Why</summary>

* We're hitting most of the dragons anyway
* Resonance will be handy as tank gets first dibs on defiant plate

</details>

<details>
<summary>Notes</summary>

Started the epic before I started this doc, which is why that section is blank. If I decide to share this I'll probably fill it out.

</details>

<details>
<summary>Checklist</summary>

- [x] 5 - Maestro's Symphony Page 24 Top
    - [x] 1
    - [x] 2
    - [x] 3
    - [x] 4
- [x] 12 - Maestro's Symphony Page 24 Bottom
    - [x] 6
    - [x] 7
    - [x] 8
    - [x] 9
    - [x] 10
    - [x] 11
- [ ] 16 - Maestro's Symphony Page 25 - South Karana from Kelkim Menkia (Give all items)
    - [ ] 13 - **Onyx Drake Gut** - Rathe Mountains from Blackwing
    - [ ] 14 - **Red Wurm Gut** - Burning Woods from Nezekezena or Phurzikon
    - [ ] 15 - **Chromatic Gut** - Skyfire Mountains from Eldrig the Old
- [ ] 26 - Mystical Lute - Steamfont Mountains from Forpar Fizfla (Give lute head, lute body, undead strings)
    - [ ] 17 - **Alluring Horn** - Ocean of Tears from Quag Maelstrom
    - [ ] 18 - Note to Forpar Fizfla - Butcherblock Mountains from Vedico Windwisper (Give Alluring Horn)
    - [ ] 19 - Forpar's Note to Himself - Steamfont Mountains from Forpar Fizfla (Give Note, say "component")
    - [ ] 20 - **Kedge Backbone** - Kedge Keep from Phingel Autropos
    - [ ] 21 - **Amygdalan Tendril** - Plane of Fear from Amygdalan
    - [ ] 22 - Petrified Werewolf Skull - Karnor's Castle from Drolvarg Warlord
    - [ ] 23 - Mystical Lute Head - Steamfont Mountains from Forpar Fizfla (Give note, backbone, tendril, skull)
    - [ ] 24 - Get the following
        - [ ] **Undead Dragongut Strings** - Trakanon
        - [ ] **White Dragon Scales** - Lady Vox or Gorenaire
        - [ ] **Red Dragon Scales** - Lord Nagafen, Talendor, or Echo of Nortlav
    - [ ] 25 - **Metal Bits** - Blacksmithing
    - [ ] Mystical Lute Body - Steamfont Mountains from Forpar Fizfla (Give white scale, red scale, metal bits)
- [ ] 27 - Singing Short Sword - Dreadlands from Baldric Slezaf (Give maestro's symphony 24 top, 24 bottom, 25, and lute)

</details>
